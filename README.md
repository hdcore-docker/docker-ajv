# HDCore - docker-ajv

## Introduction

This is a small container image that contains a Node environment with ajv. This container is very usefull for automatic testing during CI.

## Image usage

- Run ajv:

```bash
docker run --rm -v /path/to/code:/code hdcore/docker-ajv:<version> ajv.sh <arguments>
```

- Run shell:

```bash
docker run -it --rm -v /path/to/code:/code hdcore/docker-ajv:<version> /bin/sh
```

- Use in .gitlab-ci.yml:

```bash
image: hdcore/docker-ajv:<version>
script: ajv.sh <arguments>
```

## Available tags

- hdcore/docker-ajv:8

## Container Registries

The image is stored on multiple container registries at dockerhub and gitlab:

- Docker Hub:
  - hdcore/docker-ajv
  - registry.hub.docker.com/hdcore/docker-ajv
- Gitlab:
  - registry.gitlab.com/hdcore-docker/docker-ajv

## Building image

Build:

```bash
docker build -f <version>/Dockerfile -t docker-ajv:<version> .
```
