#!/bin/sh

# Author: Danny Herpol <hdcore@lachjekrom.com>
# Version GIT: 2023-01-08 11:47

# docker-entrypoint.sh
# for hdcore docker-ajv image

CGREEN="\e[32m"
CNORMAL="\e[0m"

# shellcheck disable=SC2059
printf "== ${CGREEN}Start entrypoint ${0}${CNORMAL} ==\n"

printf "HDCore docker-jsonlint container image\n"
printf "Installed version of ajv / ajv-cli: "
npm --prefix /ajv list

# shellcheck disable=SC2059
printf "== ${CGREEN}End entrypoint ${0}${CNORMAL} ==\n"

# Execute docker CMD
exec "$@"