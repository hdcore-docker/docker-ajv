#!/bin/sh

# Author: Danny Herpol <hdcore@lachjekrom.com>
# Version GIT: 2023-01-08 11:47

# ajv.sh
# for hdcore docker-ajv image

# shellcheck disable=SC2068 # double quote not allowed due to extra arguments expansion
ajv $@

